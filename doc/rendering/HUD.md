- Scaling (configurable)
- Movable (configurable)
- Opacity (configurable)
- Custom Atlas Positions (defined in json files, also for multiple versions)
- Multi resolution
- Abstract APIs (easy modding)
- Overlays (like progress bars)

## GUI Components

- Screen: Blocking modal like things (Inventories, confirmations, etc)
- Overlays
- Switch
- TextBox (with a formatter, etc)
- Buttons
- Check boxes
- Labels
- Item box
- Image arrays (for hp, etc)
- Slider
- Image
- Progress bars
- Text flows
- Selection list
- Lists

/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.minosoft.data.registries.blocks

import de.bixilon.minosoft.util.KUtil.asResourceLocation

object DefaultBlocks {
    val SCAFFOLDING = "minecraft:scaffolding".asResourceLocation()
    val LADDER = "minecraft:ladder".asResourceLocation()
    val VINE = "minecraft:vine".asResourceLocation()
    val WEEPING_VINES = "minecraft:weeping_vines".asResourceLocation()
    val WEEPING_VINES_PLANT = "minecraft:weeping_vines_plant".asResourceLocation()
    val TWISTING_VINES = "minecraft:twisting_vines".asResourceLocation()
    val TWISTING_VINES_PLANT = "minecraft:twisting_vines_plant".asResourceLocation()
    val CAVE_VINES = "minecraft:cave_vines".asResourceLocation()
    val CAVE_VINES_PLANT = "minecraft:cave_vines_plant".asResourceLocation()

    val POWDER_SNOW = "minecraft:powder_snow".asResourceLocation()
    val GRASS_BLOCK = "minecraft:grass_block".asResourceLocation()
    val MOVING_PISTON = "minecraft:moving_piston".asResourceLocation()
    val COBWEB = "minecraft:cobweb".asResourceLocation()

    val WATER = "minecraft:water".asResourceLocation()
    val BUBBLE_COLUMN = "minecraft:bubble_column".asResourceLocation()
}

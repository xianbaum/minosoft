/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.minosoft.data.registries.enchantment

import de.bixilon.minosoft.util.KUtil.asResourceLocation

object DefaultEnchantments {
    val SOUL_SPEED = "minecraft:soul_speed".asResourceLocation()
    val UNBREAKING = "minecraft:unbreaking".asResourceLocation()
    val EFFICIENCY = "minecraft:efficiency".asResourceLocation()
    val AQUA_AFFINITY = "minecraft:aqua_affinity".asResourceLocation()
    val DEPTH_STRIDER = "minecraft:depth_strider".asResourceLocation()
}
